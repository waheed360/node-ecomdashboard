const dateFns = require('date-fns');
const Braintree = require('braintree');

const Models = require('../models');
const defer = require('../lib/defer-promise');

const RESULT_CHUNK_SIZE = 1000;

const braintree = Braintree.connect({
  clientId: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
});

module.exports = {
  queueName: 'braintree',
  jobTypes: {

    async import(ctx, jobs) {
      // TODO: validate the params are correct
      const {
        externalAccountId, createdAtMax,
      } = ctx;

      const settledAtMin = new Date(ctx.settledAtMin);
      const settledAtMax = new Date(ctx.settledAtMax);

      // TODO: throw an error if start date after end date

      const externalAccount = await Models.externalAccount.findByPk(externalAccountId);
      if (!externalAccount) ctx.throwAndDie('NotFound', 'Invalid external account id');
      const { refreshToken, expiresAt } = externalAccount.auth;
      // // check if token will expire in next 5 minutes
      if (new Date(expiresAt) < dateFns.addMinutes(new Date(), 5)) {
        const tokenResult = await braintree.oauth.createTokenFromRefreshToken({ refreshToken });
        await externalAccount.update({ auth: tokenResult.credentials });
      }
      const { accessToken } = externalAccount.auth;

      // if the date range is too big (current threshold is 1 day) split up the job
      const numDays = dateFns.differenceInCalendarDays(settledAtMax, settledAtMin);
      if (numDays > 1) {
        let splitMax = settledAtMax;
        let numJobs = 0;

        while (splitMax > settledAtMin) {
          numJobs++;
          const dayBeforeSplitMax = dateFns.subDays(splitMax, 1);
          // const jobs = kue.createQueue(redisConfig);

          const createJobDone = defer();
          jobs
            .create('braintree', {
              externalAccountId,
              settledAtMin: dayBeforeSplitMax,
              settledAtMax: splitMax,
            })
            .removeOnComplete(true)
            // eslint-disable-next-line no-loop-func
            .save((err) => {
              if (err !== null) {
                createJobDone.reject(err);
                return;
              }
              createJobDone.resolve();
            });
          // eslint-disable-next-line no-await-in-loop
          await createJobDone.promise;

          splitMax = dayBeforeSplitMax;
        }
        ctx.result = { numJobs };
      }

      // now actually import the transactions from the date range
      // which will trigger more jobs for chunks of results
      const userGateway = Braintree.connect({ accessToken });
      const streamDone = defer();

      // the search results filter is not super precise
      // there will be extra results from just before the cutoff
      // seems to be 20 extra results
      const transactionsStream = await userGateway.transaction.search((search) => {
        search.settledAt().between(settledAtMin, settledAtMax);
        if (createdAtMax) search.createdAt().max(createdAtMax);
      });
      let reachedResultsCutoff = false;
      const txns = [];
      transactionsStream.on('data', async (btTransaction) => {
        const txn = btTransaction;
        console.log(btTransaction.amount);
        const ecomTransaction = {
          id: `${externalAccountId}_${txn.id}`,
          externalAccountId: `${externalAccountId}`,
          amount: txn.type === 'credit' ? -1 * txn.amount : txn.amount,
          details: txn,
        };
        txns.push(ecomTransaction);
        await Models.ecomTransaction.upsert(ecomTransaction);

        if (txns.length >= RESULT_CHUNK_SIZE) {
          // hacky method to tell braintree to stop the datastream
          // invoking transactionsStream.destroy() alone does NOT work
          transactionsStream.pause();
          transactionsStream.readyToStart = null;

          reachedResultsCutoff = true;
          streamDone.resolve();
        }
      }).on('error', async (err) => {
        streamDone.reject(err);
      }).on('end', async () => {
        // gets called if we reach the end of the results or on destroy
        if (!reachedResultsCutoff) streamDone.resolve();
      });

      await streamDone.promise;

      // enqueue the next job to get the next chunk of results
      if (reachedResultsCutoff) {
        console.log('reached the cutoff');
        // the search results come in from newest to oldest
        const createNewJob = defer();
        jobs
          .create('braintree', {
            externalAccountId,
            settledAtMin,
            settledAtMax,
            createdAtMax: new Date(txns[txns.length - 1].details.createdAt),
          })
          .removeOnComplete(true)
          .save((err) => {
            if (err !== null) {
              createNewJob.reject(err);
              return;
            }
            createNewJob.resolve();
          });
        await createNewJob.promise();
      }
    },
  },
};


// TODO webhooks, or nightly jobs?
