const kue = require('kue');
const braintree = require('../queues/braintree');

let redisConfig;

const jobs = kue.createQueue(redisConfig);

// const queue = kue.createQueue(redisConfig);
jobs.process('braintree', (jobPayload, done) => {
  console.log(`processing job ${jobPayload.id}`);
  braintree.jobTypes.import(jobPayload.data, jobs);
  done(null, 'Done');
});

kue.app.listen(8025); // process.env.KUE_PORT
kue.app.set('title', 'Kue');

module.exports = jobs;
