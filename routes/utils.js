// const { enqueueJob } = require('../lib/queue');
const Router = require('koa-router');
const jobs = require('../lib/jobs');

const router = new Router();

router.get('/run/:queueName/:jobName', (ctx, res) => {
  const queueName = ctx.params.queueName;

  const job = jobs.create(queueName, ctx.query);

  job.on('complete', (result) => {
    console.log(`completed job ${job.id}`);
  });

  job.save((err) => {
    if (err) {
      console.log(`failed to queue job ${job.id}`);
    } else {
      console.log(`queued job ${job.id}`);
    }
  });

  ctx.body = 'Job enqueued';
});

module.exports = router;
