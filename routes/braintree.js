const nconf = require('nconf');
const _ = require('lodash');
const dateFns = require('date-fns');
const Router = require('koa-router');
const Braintree = require('braintree');

// const router = require('express').Router();
const payment = require('../queues/braintree.js');
const Models = require('../models');

const router = new Router();

// const braintree = Braintree.connect({
//   clientId: 'client_id$sandbox$64pm3wq7c85dm7ht',
//   clientSecret: 'client_secret$sandbox$5d998053c9273b39d8b54a9b1e5737e4',
// });

router.post('/createExternalAccount', async (ctx, next) => {
  const gateway = Braintree.connect({
    // TODO: make these environment variables.
    clientId: 'client_id$sandbox$kcg96pypnng5xpwz',
    clientSecret: 'client_secret$sandbox$1492d9182c2d2dd577ea3f23ddc1dfb7',
  });
  const { state, merchantId, code } = ctx.request.body;
  const tokenResult = await gateway.oauth.createTokenFromCode({ code });

  console.log(tokenResult);

  // TODO: check whether the external account already exists and raise exception
  await Models.externalAccount.upsert({
    id: `braintreee+${merchantId}`,
    auth: tokenResult.credentials,
  });
  // const what = await Models.externalAccount.findByPk(1);
  ctx.body = { Status: 'True' };
});

// module.exports = function initRoutes(router) {
//
//
//   // router.get('/test', (req, res, next) => {
//   //   const order = req.body;
//   // });
// };
module.exports = router;
