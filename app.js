const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const cors = require('koa2-cors');
const Router = require('koa-router');

const app = new Koa();

const kue = require('kue');
const utils = require('./routes/utils');
const braintree = require('./routes/braintree');

app.use(cors());
app.use(bodyParser());

// all routes here
app.use(utils.routes());
app.use(braintree.routes());

app.listen(3000);

console.log('Server running on port 3000');
