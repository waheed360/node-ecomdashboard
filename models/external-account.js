
module.exports = (sequelize, DataTypes) => {
  const externalAccount = sequelize.define('externalAccount', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    auth: DataTypes.JSON,
  }, { tableName: 'external_accounts' });

  return externalAccount;
};
