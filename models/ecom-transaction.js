
module.exports = (sequelize, DataTypes) => {
  const ecomTransaction = sequelize.define('ecomTransaction', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    amount: DataTypes.INTEGER,
    details: DataTypes.JSON,
    externalAccountId: {
      type: DataTypes.INTEGER,
      references: 'externalAccounts',
      referencesKey: 'id',
    },
  }, { tableName: 'ecom_transactions' });
  return ecomTransaction;
};
