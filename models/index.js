/* eslint-disable global-require,import/no-dynamic-require */
const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

const basename = path.basename(__filename);
const Models = {};

const sequelize = new Sequelize(
  'ecommerce',
  null, null,
  {
    dialect: 'postgres',
    operatorsAliases: false,
    logging: false,
  },
);

fs
  .readdirSync(__dirname)
  .filter((file) => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file));
    Models[model.name] = model;
  });

Object.keys(Models).forEach((modelName) => {
  if (Models[modelName].associate) {
    Models[modelName].associate(Models);
  }
});

Models.sequelize = sequelize;
Models.Sequelize = Sequelize;

module.exports = Models;
